package mysql;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
@RequestMapping(path = "/demo")
public class MainController {
    @Autowired
    private UserRepository userRepository;

    /*
    * CREATE
    * */
    @PostMapping(path="/add")
    @CrossOrigin
    public @ResponseBody String addNewUser(@RequestBody User user) {
        User n = user;
        userRepository.save(n);
        return "{\"msg\": \"User Saved\"}";
    }

    /*
    * READ
    * */
    @CrossOrigin
    @GetMapping(path="/all")
    public @ResponseBody Iterable<User> getAllUsers() {
        return userRepository.findAll();
    }

    /*
     * UPDATE
     */
    @GetMapping(path="/update")
    @CrossOrigin
    public @ResponseBody String updateUser(@RequestParam Integer id,
                                         @RequestParam(required = false) String name,
                                         @RequestParam(required = false) String email) {
        Optional<User> userToUpdate = userRepository.findById(id);
        if (userToUpdate.isPresent()) {
            User u = userToUpdate.get();
            if (name != null) {
                u.setName(name);
            }
            if (email != null) {
                u.setEmail(email);
            }
            userRepository.save(u);
            return "{\"msg\": \"User Updated\"}";
        }
        return "User with given iD not found";
    }

    /*
    * DELETE
    * */
    @DeleteMapping(path="/delete")
    @CrossOrigin
    public @ResponseBody String deleteUser(@RequestParam Integer id) {
        Optional<User> userToDelete = userRepository.findById(id);
        if (userToDelete.isPresent()){
            userRepository.delete(userToDelete.get());
        } else {
            return "User with given ID not Found\n";
        }
        return "{\"msg\": \"deleted\"}";
    }
}
